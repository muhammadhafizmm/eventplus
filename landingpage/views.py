from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.contrib.auth.forms import UserCreationForm
from django.db import connection
import datetime

def index(request):
    cursor = connection.cursor()
    # cursor.execute("select * from transaksi")
    # row = cursor.fetchall()
    if request.user.is_authenticated:
        email = request.user.username
        cursor.execute("select * from organizer where email='"+email+"'")
        dataUser = cursor.fetchall()
        print(dataUser)
        if dataUser == []:
            tipe = "Pengguna"
            return render(request, "landingpage/index.html", {'tipe':tipe})
        else:
            tipe = "Organizer"
            cursor.execute("select e.id_event, e.nama, e.tanggal_mulai, e.tanggal_selsai, e.jam_mulai, e.jam_selesai, e.deskripsi, e.kapasitas_total, e.kapasitas_total_tersedia, t.nama_tipe from event e, tipe t where email_organizer='"+email+"' and e.id_tipe = t.id_tipe")
            dataEvent = cursor.fetchall()
            print(dataEvent)
            return render(request, "landingpage/index.html", {'tipe':tipe, 'dataEvent':dataEvent})
    return render(request, "landingpage/index.html")

def event(request):
    cursor = connection.cursor()
    cursor.execute("select e.id_event, e.nama, e.tanggal_mulai, e.tanggal_selsai, e.jam_mulai, e.jam_selesai, e.deskripsi, e.kapasitas_total, e.kapasitas_total_tersedia, t.nama_tipe, e.email_organizer from event e, tipe t where e.id_tipe = t.id_tipe")
    row = cursor.fetchall()
    return render(request, "landingpage/event.html", {'row': row})

def tambahevent(request):
    if request.user.is_authenticated:
        email = request.user.username
        if request.method == "POST":
            name = request.POST['name']
            tglmulai = request.POST['tglmulai']
            tglselesai = request.POST['tglselesai']
            jammulai = request.POST['jammulai']
            jamselesai = request.POST['jamselesai']
            deskripsi = request.POST['deskripsi']
            tipe = request.POST['tipe']
            cursor = connection.cursor()
            cursor.execute("INSERT INTO event (nama, tanggal_mulai, tanggal_selsai, jam_mulai, jam_selesai, deskripsi, kapasitas_total, kapasitas_total_tersedia, id_tipe, email_organizer) VALUES ('"+name+"', '"+tglmulai+"', '"+tglselesai+"', '"+jammulai+"', '"+jamselesai+"', '"+deskripsi+"', 0, 0, '"+tipe+"', '"+email+"');")
            return redirect("../")
        else:
            cursor = connection.cursor()
            cursor.execute("select * from tipe")
            row = cursor.fetchall()
        return render(request, "landingpage/tambahevent.html", {'row': row})

def pilihTipe(request):
    return render(request, "landingpage/pilihTipe.html")

def registerPengunjung(request):
    if request.method == 'POST':
        print(request.POST)
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            cursor = connection.cursor()
            email = request.POST['username']
            password = request.POST['password1']
            namaDepan = request.POST['namaDepan']
            namaBelakang = request.POST['namaBelakang']
            cursor.execute(f"INSERT INTO pengguna (email, password) VALUES ('{email}', '{password}')")
            cursor.execute(f"INSERT INTO pengunjung (email, nama_depan, nama_belakang) VALUES ('{email}', '{namaDepan}', '{namaBelakang}')")
            username = form.cleaned_data.get('username')
            messages.success(request, f'Selamat, Akun {username} telah dibuat!')
            print(username)
            return redirect('landingpage:login')
        else:
            messages.error(request, 'Wahh sepertinya ada yang salah di pengisian data mu!')
            return render(request, 'landingpage/registerPengunjung.html', {"form" : form})
    else:
        form = UserCreationForm()
        return render(request, "landingpage/registerPengunjung.html", {"form" : form})

def registerOrganizer(request):
    if request.method == 'POST':
        print(request.POST)
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            cursor = connection.cursor()
            email = request.POST['username']
            password = request.POST['password1']
            namaDepan = request.POST['namaDepan']
            nomor = request.POST['nomor']
            tipeOrgan = request.POST.get('tipeOrgan', False)
            cursor.execute(f"INSERT INTO pengguna (email, password) VALUES ('{email}', '{password}')")
            cursor.execute(f"INSERT INTO organizer (email, npwp) VALUES ('{email}', '{nomor}')")
            if (tipeOrgan == "individu"):
                namaBelakang = request.POST['namaBelakang']
                cursor.execute(f"INSERT INTO individu (email, no_ktp, nama_depan, nama_belakang) VALUES ('{email}', '{nomor}', '{namaDepan}', '{namaBelakang}')")
            elif (tipeOrgan == "perusahaan"):
                cursor.execute(f"INSERT INTO perusahaan (email, nama) VALUES ('{email}', '{namaDepan}')")
            username = form.cleaned_data.get('username')
            messages.success(request, f'Selamat, Akun {username} telah dibuat!')
            print(username)
            username = form.cleaned_data.get('username')
            messages.success(request, f'Selamat, Akun {username} telah dibuat!')
            print(username)
            return redirect('landingpage:login')
        else:
            messages.error(request, 'Wahh sepertinya ada yang salah di pengisian data mu!')
            return render(request, 'landingpage/registerOrganizer.html', {"form" : form})
    else:
        form = UserCreationForm()
        return render(request, "landingpage/registerOrganizer.html", {"form" : form})

def loginSuccessMessage(sender, user, request, **kwargs):
    messages.success(request, "Halo " + user.username + "! Kamu berhasil Login.")

def logoutSuccessMessage(sender, user, request, **kwargs):
    messages.success(request, "Terimakasih " + user.username + " telah berkunjung. Dadah!")

user_logged_in.connect(loginSuccessMessage)
user_logged_out.connect(logoutSuccessMessage)