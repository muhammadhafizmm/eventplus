from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

app_name = "landingpage"
urlpatterns = [
    path('', views.index, name='index'),
    path('accounts/login/', auth_views.LoginView.as_view(template_name='landingpage/login.html'), name='login'),
    path('accounts/logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('accounts/register/', views.pilihTipe, name='pilihTipe'),
    path('accounts/register/organizer', views.registerOrganizer, name='registerOrginazer'),
    path('accounts/register/pengunjung', views.registerPengunjung, name='registerPengunjung'),
    path('event/', views.event, name='event'),
    path('tambahevent/', views.tambahevent, name='tambahevent'),
    # path('tambahkelastiket/', views.tambahkelastiket, name='tambahkelastiket')
]